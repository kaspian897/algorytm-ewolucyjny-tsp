﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Randomizations;
using GeneticSharp.Domain.Fitnesses;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Mutations;
using System.Globalization;
using System.Collections;
using GeneticSharp.Domain.Randomizations;

namespace MetodyH2
{
    class VertexCover
    {
        public class VertexCoverChromosome : ChromosomeBase
        {
            private readonly int nodes;
            private readonly int k;

            public VertexCoverChromosome(int nodes,int k) : base(k)
            {
                this.nodes = nodes;
                this.k = k;
                var nodesIndexes = RandomizationProvider.Current.GetUniqueInts(k, 0, nodes);
                for (int i = 0; i < k; i++)
                {
                    ReplaceGene(i, new Gene(nodesIndexes[i]));
                }
            }
            public double Cost { get; internal set; }
            public override Gene GenerateGene(int geneIndex)
            {
                return new Gene(RandomizationProvider.Current.GetInt(0, nodes));
            }
            public override IChromosome CreateNew()
            {
                return new VertexCoverChromosome(nodes, k);
            }
            public override IChromosome Clone()
            {
                var clone = base.Clone() as VertexCoverChromosome;

                clone.Cost = Cost;
                return clone;
            }
            public override string ToString()
            {
                return base.ToString();
            }
        }


        public class VertexCoverFitness : IFitness
        {
            ArrayList nodes;
            HashSet<Edge> edges;
            public VertexCoverFitness(ArrayList nodes, HashSet<Edge> edges)
            {
                this.nodes = nodes;
                this.edges = edges;


            }

            public double Evaluate(IChromosome chromosome)
            {
                var genes = chromosome.GetGenes();
                //var lastCityIndex = Convert.ToInt32(genes[0].Value, CultureInfo.InvariantCulture);
                //var citiesIndexes = new List<int>();
                //citiesIndexes.Add(lastCityIndex);

                HashSet<Edge> covered = new HashSet<Edge>();
                // Calculates the total route distance.
                int vc = 0;
                foreach (var g1 in genes)
                {
                    var index1 = Convert.ToInt32(g1.Value, CultureInfo.InvariantCulture);
                    covered.UnionWith(((Node)nodes[index1]).edges);
                    //Console.WriteLine(index1+"");
                    //foreach (var item in ((Node)nodes[index1]).edges)
                   // {
                   //     //Console.WriteLine(item);
                    //}
                    //foreach (var item in covered)
                    //{
                    //    Console.WriteLine(item);
                    //}
                    //Console.WriteLine();
                    /*foreach (Node g2 in nodes)
                    {
                        var index1 = Convert.ToInt32(g1.Value, CultureInfo.InvariantCulture);
                        var index2 = Convert.ToInt32(g2.Value, CultureInfo.InvariantCulture);
                       // Console.WriteLine(index1.ToString()+" "+index2.ToString());
                        //bool flag = true;
                        if (index1 > index2)
                        {
                            var temp = index1;
                            index1 = index2;
                            index2 = temp;
                        }
                        if (index1 != index2)
                        {
                            var edge = new Edge(index1, index2);
                            if (edges.Contains(edge))
                            {
                                vc++;
                                //Console.WriteLine(edge);
                            }
                            //else
                            //{
                                //vc++;
                            //}
                        }

                    }*/

                    
                }
                vc = covered.Count;
                //Console.WriteLine(vc+ " " + edges.Count+" "+(edges.Count - vc));

                
                //Console.WriteLine(vc);
                var fitness = (edges.Count - vc);
                ((VertexCoverChromosome)chromosome).Cost = fitness;
                return vc;
            }
        }
        public class VertexCoverCrossover: ICrossover
        {

            public VertexCoverCrossover()
            {
            }

            int ICrossover.ParentsNumber =>2;

            int ICrossover.ChildrenNumber => 1;

            int ICrossover.MinChromosomeLength => 1;

            bool IChromosomeOperator.IsOrdered => false;

            public IList<IChromosome> Cross(IList<IChromosome> parents)
            {
                IList<IChromosome> vertexCoverChromosomes = new List<IChromosome>();
                VertexCoverChromosome par1= parents.ElementAt<IChromosome>(0) as VertexCoverChromosome;
                VertexCoverChromosome par2= parents.ElementAt<IChromosome>(1) as VertexCoverChromosome;
                var inter=par1.GetGenes().Intersect(par2.GetGenes());
                var inter1=par1.GetGenes().Except(inter).ToArray();
                var inter2=par2.GetGenes().Except(inter).ToArray();


                //Shuffle(inter1);
                //Shuffle(inter2);
                int percent50 = (int)(0.5 * Math.Min(inter1.Count(), inter2.Count()));
                var genesIndexes1 = RandomizationProvider.Current.GetUniqueInts(percent50, 0, inter1.Length);
                var genesIndexes2 = RandomizationProvider.Current.GetUniqueInts(percent50, 0, inter2.Length);

                //var genes1 = inter1.Clone();
                //var genes2 = inter1.Clone();
                //Gene[] genes = new Gene[(int)( 0.5 *Math.Min(inter1.Count(), inter2.Count()))];
                
                for (int i = 0; i < percent50; i++)
                {
                    //Console.WriteLine(inter1[genesIndexes1[i]]+" "+ inter2[genesIndexes2[i]]);
                    inter1[genesIndexes1[i]] = inter2[genesIndexes2[i]];
                    //child.ReplaceGene()

                }
                var child = inter.Union(inter1);
                //var child1 = inter.Union(genes).ToList();
                //int count = child1.Count();
                // if (count < par1.Length)
                //{
                //    for (int i = count; i < par1.Length; i++)
                //    {
                //        child1.Add(inter1[i- count]);
                //    }
                //}
                //Shuffle(child1);
                /*
                foreach (var item in par1.GetGenes())
                {
                    Console.WriteLine(item.Value);
                }
                Console.WriteLine();
                foreach (var item in par2.GetGenes())
                {
                    Console.WriteLine(item.Value);
                }
                Console.WriteLine();
                foreach (var item in child)
                {
                    Console.WriteLine(item.Value);
                }

                Console.WriteLine();
                Console.WriteLine();
                */
                var out1 = par1.Clone();
                out1.ReplaceGenes(0, child.ToArray());
                vertexCoverChromosomes.Add(out1);
                //vertexCoverChromosomes.Add(child2);
                return vertexCoverChromosomes;
            }
            public void Shuffle(IList<Gene> list)
            {
                int n = list.Count;
                Random rng = new Random();
                while (n > 1)
                {
                    n--;
                    int k = rng.Next(n + 1);
                    Gene value = list[k];
                    list[k] = list[n];
                    list[n] = value;
                }
            }


        }
        public class VertexCoverMutation : IMutation
        {
            public int nodesCount;
            public HashSet<Gene> nodesSet;
            ArrayList nodes;
            HashSet<Edge> edges;
 
            public VertexCoverMutation(int nodesCount, ArrayList nodes, HashSet<Edge> edges)
            {
                this.nodesCount = nodesCount;
                this.nodesSet = new HashSet<Gene>();
                this.nodes = nodes;
                this.edges = edges;
                for (int i = 0; i < nodesCount; i++)
                {
                    this.nodesSet.Add(new Gene(i));
                }
                
            }

            public bool IsOrdered => false;

            public void Mutate(IChromosome chromosome, float probability)
            {
                //Random rnd = new Random();
                

                double choice=RandomizationProvider.Current.GetDouble(0, 1);
                if (choice < 0.5)
                {
                    var genes = chromosome.GetGenes();
                    var diff = nodesSet.Except(genes).ToArray();
                    int percent5 = (int)(genes.Length * 0.05);
                    percent5 = Math.Min(percent5, diff.Length);
                    var genesIndexes = RandomizationProvider.Current.GetUniqueInts(percent5, 0, genes.Length);
                    var diffIndexes = RandomizationProvider.Current.GetUniqueInts(percent5, 0, diff.Length);
                    for (int i = 0; i < percent5; i++)
                    {
                        chromosome.ReplaceGene(genesIndexes[i], diff[diffIndexes[i]]);
                    }
                }
                else
                {
                    var edges = new HashSet<Edge>();
                    foreach (var item in chromosome.GetGenes())
                    {
                        var j = Convert.ToInt32(item.Value, CultureInfo.InvariantCulture);
                        edges.UnionWith(((Node)nodes[j]).edges);
                    }
                    var notCovered = this.edges.Except(edges);
                    var genes = chromosome.GetGenes();
                    int percent5 = (int)(genes.Length * 0.05);


                    var genesIndexes = RandomizationProvider.Current.GetUniqueInts(percent5, 0, genes.Length);
                    int i = 0;
                    while (i < percent5)
                    {
                        var asArray = notCovered.ToArray();
                        if (asArray.Length <= 0)
                        {
                            break;
                        }
                        int index=RandomizationProvider.Current.GetInt(0, asArray.Length);

                        Node node1 = nodes[asArray[index].start] as Node;
                        Node node2 = nodes[asArray[index].end] as Node;

                        double choiceNode = RandomizationProvider.Current.GetDouble(0, 1);
                        if (choiceNode < 0.5)
                        {
                            chromosome.ReplaceGene(genesIndexes[i], new Gene(asArray[index].start));
                            notCovered=notCovered.Except(node1.edges);
                        }
                        else
                        {
                            chromosome.ReplaceGene(genesIndexes[i], new Gene(asArray[index].end));
                            notCovered=notCovered.Except(node2.edges);
                        }

                        i++;
                    } 
                    var diffIndexes = RandomizationProvider.Current.GetUniqueInts(percent5, 0, genes.Length);
                }

            }
        }

    }
}
