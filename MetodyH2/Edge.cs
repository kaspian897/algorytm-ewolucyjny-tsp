﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodyH2
{
    class Edge : IEquatable<Edge>
    {

        public int start;
        public int end;
        public Edge(int start, int end)
        {
            //this.weight = weight;
            this.start = start;
            this.end = end;
        }
        public override int GetHashCode()
        {
            return 7919 * start + end;
           // return start.GetHashCode() ^ end.GetHashCode();
        }
        public bool Equals(Edge other)
        {
            if(this.start== other.start&& this.end == other.end)
            {
                return true;
            }
            if (this.start == other.end && this.end == other.start)
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return start+" "+end;
        }
    }
}
