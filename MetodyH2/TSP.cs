﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Randomizations;
using GeneticSharp.Domain.Fitnesses;
using System.Globalization;
using System.Collections;

namespace MetodyH2
{
    class TSP
    {
        public class TspChromosome : ChromosomeBase
        {
            private readonly int nodes;

            public TspChromosome(int nodes) : base(nodes)
            {
                this.nodes = nodes;
                var nodesIndexes = RandomizationProvider.Current.GetUniqueInts(nodes, 0, nodes);
                for (int i = 0; i < nodes; i++)
                {
                    ReplaceGene(i, new Gene(nodesIndexes[i]));
                }
            }
            public double Distance { get; internal set; }
            public override Gene GenerateGene(int geneIndex)
            {
                return new Gene(RandomizationProvider.Current.GetInt(0, nodes));
            }
            public override IChromosome CreateNew()
            {
                return new TspChromosome(nodes);
            }
            public override IChromosome Clone()
            {
                var clone = base.Clone() as TspChromosome;

                clone.Distance = Distance;
                return clone;
            }
        }
        public class TspFitness : IFitness
        {
            ArrayList nodes;
            //public IList<Node> Cities { get; private set; }
            public TspFitness(ArrayList nodes)
            {
                this.nodes = nodes; 


            }
            
            public double Evaluate(IChromosome chromosome)
            {
                var genes = chromosome.GetGenes();
                var distanceSum = 0.0;
                var lastCityIndex = Convert.ToInt32(genes[0].Value, CultureInfo.InvariantCulture);
                var citiesIndexes = new List<int>();
                citiesIndexes.Add(lastCityIndex);
                // Calculates the total route distance.
                foreach (var g in genes)
                {
                    var currentCityIndex = Convert.ToInt32(g.Value, CultureInfo.InvariantCulture);
                    distanceSum += CalcDistanceTwoNodes((Node)nodes[currentCityIndex],(Node) nodes[lastCityIndex]);

                    lastCityIndex = currentCityIndex;
                    citiesIndexes.Add(lastCityIndex);
                }
                distanceSum += CalcDistanceTwoNodes((Node)nodes[citiesIndexes.Last()], (Node)nodes[citiesIndexes.First()]);
                
                var fitness = 1.0 - (distanceSum / (nodes.Count * 1000.0));
                ((TspChromosome)chromosome).Distance = distanceSum;

                var diff = nodes.Count - citiesIndexes.Distinct().Count();
                if (diff > 0)
                {
                    fitness /= diff;
                }
                if (fitness < 0)
                {
                    fitness = 0;
                }
                return fitness;
            }
            private static double CalcDistanceTwoNodes(Node one, Node two)
            {
                return Math.Sqrt(Math.Pow(one.point.x- two.point.x,2)+ Math.Pow(one.point.y - two.point.y, 2));
            }
        }

    }
}
