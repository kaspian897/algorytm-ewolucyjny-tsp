﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GeneticSharp.Domain.Chromosomes;
using System.Threading;
using GeneticSharp.Domain;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Populations;
using GeneticSharp.Domain.Selections;
using GeneticSharp.Domain.Terminations;
using GeneticSharp.Infrastructure.Framework.Threading;
namespace MetodyH2
{
     partial class Form1 : Form
    {
        Graph g;
        private GeneticAlgorithm m_ga;

        private Thread m_gaThread;
        private Thread t;
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 1;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {

               // Point point1 = new Point(10, 10);
              //  Point point2 = Point.Add(point1, new Size(250, 0));
              //  e.Graphics.DrawLine(Pens.Red, point1, point2);
            
        }

        private void generuj_Click(object sender, EventArgs e)
        {
            
            int edges = Convert.ToInt32(textBox3.Text);
            int verts = Convert.ToInt32(textBox4.Text);
            g = new Graph(verts, edges, panel1.Width, panel1.Height);

            Graphics gr = panel1.CreateGraphics();
            gr.Clear(panel1.BackColor);
            Pen pen = new Pen(Color.White, 2);
            SolidBrush redBrush = new SolidBrush(Color.Red);
            foreach (var item in g.nodes)
            {
                Node n = item as Node;
                gr.FillEllipse(redBrush, (int)n.point.x, (int)n.point.y, 10, 10);

            }
            if (comboBox1.SelectedIndex == 0)
            {
                
            }else if (comboBox1.SelectedIndex == 1)
            {

                foreach (var item in g.edges)
                {
                    Node n1 = g.nodes[item.start] as Node;
                    Node n2 = g.nodes[item.end] as Node;
                    gr.DrawLine(Pens.Red,(int) n1.point.x+5, (int)n1.point.y+5, (int)n2.point.x+5, (int)n2.point.y+5);
                    //gr.draw(redBrush, (int)n.point.x, (int)n.point.y, 10, 10);
                }
            }

        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            int gener = Convert.ToInt32(textBox5.Text);
            int popmin = Convert.ToInt32(textBox1.Text);
            int popmax = Convert.ToInt32(textBox2.Text);
            if (comboBox1.SelectedIndex == 0)
            {
                var fitness = new TSP.TspFitness(g.nodes);
                var chromosome = new TSP.TspChromosome(g.nodes.Count);
                var crossover = new OrderedCrossover();
                var mutation = new ReverseSequenceMutation();
                var selection = new RouletteWheelSelection();
                var population = new Population(popmin, popmax, chromosome);
                m_ga = new GeneticAlgorithm(population, fitness, selection, crossover, mutation);
                m_ga.Termination = new TimeEvolvingTermination(System.TimeSpan.FromHours(1));
                m_ga.TaskExecutor = new ParallelTaskExecutor
                {
                    MinThreads = 100,
                    MaxThreads = 200
                };
                m_ga.GenerationRan += delegate
                {
                    var distance = ((TSP.TspChromosome)m_ga.BestChromosome).Distance;
                    SetText($"Generation: {m_ga.GenerationsNumber} - Distance: ${distance}");
                    if (m_ga.GenerationsNumber % 100 == 0)
                    {
                        DrawTSP((TSP.TspChromosome)m_ga.BestChromosome);
                    }
                };
                m_gaThread = new Thread(() => m_ga.Start());
                m_gaThread.Start();
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                 t=new Thread(() =>
                {
                    VertexCover.VertexCoverChromosome best = null;
                    var s1 = 0;
                    var e1 = g.nodes.Count;

                    while (s1 <= e1)
                    {
                        int k = (s1 + e1) / 2;

                        var fitness = new VertexCover.VertexCoverFitness(g.nodes, g.edges);
                        var chromosome = new VertexCover.VertexCoverChromosome(g.nodes.Count, k);
                        var crossover = new VertexCover.VertexCoverCrossover();
                        var mutation = new VertexCover.VertexCoverMutation(g.nodes.Count, g.nodes, g.edges);
                        var selection = new EliteSelection();
                        var population = new Population(popmin, popmax, chromosome);
                        m_ga = new GeneticAlgorithm(population, fitness, selection, crossover, mutation);
                        m_ga.Termination = new GenerationNumberTermination(gener);
                        m_ga.TaskExecutor = new ParallelTaskExecutor
                        {
                            MinThreads = 100,
                            MaxThreads = 200
                        };
                        m_ga.GenerationRan += delegate
                        {
                        //best = ((VertexCover.VertexCoverChromosome)m_ga.BestChromosome);
                        var cost = ((VertexCover.VertexCoverChromosome)m_ga.BestChromosome).Cost;

                            SetText($"Generation: {m_ga.GenerationsNumber} - Cost: ${cost} - K: ${k}");
                        //if (m_ga.GenerationsNumber % 100 == 0)
                        // {
                        DrawVC((VertexCover.VertexCoverChromosome)m_ga.BestChromosome);
                        // }
                    };

                        m_gaThread = new Thread(() => m_ga.Start());
                        m_gaThread.Start();
                        m_gaThread.Join();
                        //m_ga.TaskExecutor.
                        if (best == null)
                        {
                            best = (VertexCover.VertexCoverChromosome)m_ga.BestChromosome.Clone();
                        }
                        if ((int)((VertexCover.VertexCoverChromosome)m_ga.BestChromosome).Cost == 0)
                        {
                            e1 = k - 1;
                            best = (VertexCover.VertexCoverChromosome)m_ga.BestChromosome.Clone();
                        }
                        else
                        {
                            s1 = k + 1;
                        }

                    }
                    SetText($" Cost: ${best.Cost} - K: ${best.Length}");
                    DrawVC(best);
                });
                t.Start();
                /* m = int((l + h) / 2.0)
                 cv,res = environ(n, m, mut_prob, pop_init, pop_total, max_iterate, edges)
                 if (cv == 0):
         result_dict[m] = res
         h = m - 1
     else:
         l = m + 1*/
                //int k = 10;

            }
        }
        delegate void SetTextCallback(string text);
        delegate void DrawCallbackTSP(TSP.TspChromosome chromoseome);
        delegate void DrawCallbackVC(VertexCover.VertexCoverChromosome chromoseome);
        public void DrawVC(VertexCover.VertexCoverChromosome chromoseome)
        {

            try
            {
                if (this.panel1.InvokeRequired)
                {

                    DrawCallbackVC d = new DrawCallbackVC(DrawVC);

                    this.panel1.Invoke(d, new object[] { chromoseome });

                }
                else
                {
                    Graphics gr = panel1.CreateGraphics();
                    gr.Clear(panel1.BackColor);
                    Pen pen = new Pen(Color.White, 2);
                    SolidBrush redBrush = new SolidBrush(Color.Red);
                    SolidBrush blueBrush = new SolidBrush(Color.Blue);

                    var genes = chromoseome.GetGenes();
                    Node n1;
                    Node n2;
                    HashSet<Edge> blueEdges = new HashSet<Edge>();
                    foreach (var item in g.nodes)
                    {
                        
                        Node n = item as Node;
                        if (genes.Contains(new Gene(n.id)))
                        {
                            blueEdges.UnionWith(n.edges);
                            gr.FillEllipse(blueBrush, (int)n.point.x, (int)n.point.y, 10, 10);

                        }
                        else
                        {
                            gr.FillEllipse(redBrush, (int)n.point.x, (int)n.point.y, 10, 10);
                            

                        }
                            

                    }
                    foreach (var item in blueEdges)
                    {
                        //gr.FillEllipse(blueBrush, (int)n.point.x, (int)n.point.y, 10, 10);
                        //foreach (var edge in n.edges)
                        //{
                        n1 = g.nodes[(int)item.start] as Node;
                        n2 = g.nodes[(int)item.end] as Node;
                        gr.DrawLine(Pens.Blue, (int)n1.point.x + 5, (int)n1.point.y + 5, (int)n2.point.x + 5, (int)n2.point.y + 5);

                        //}

                    }
                    foreach (var item in g.edges.Except(blueEdges))
                    {
                        //gr.FillEllipse(blueBrush, (int)n.point.x, (int)n.point.y, 10, 10);
                        //foreach (var edge in n.edges)
                        //{
                        n1 = g.nodes[(int)item.start] as Node;
                        n2 = g.nodes[(int)item.end] as Node;
                        gr.DrawLine(Pens.Red, (int)n1.point.x + 5, (int)n1.point.y + 5, (int)n2.point.x + 5, (int)n2.point.y + 5);

                        //}

                    }

                }
            }
            catch (ObjectDisposedException)
            {
                m_ga.Stop();
            }

        }
        public void DrawTSP(TSP.TspChromosome chromoseome)
        {
            
            try
            {
                if (this.panel1.InvokeRequired)
                {

                    DrawCallbackTSP d = new DrawCallbackTSP(DrawTSP);

                    this.panel1.Invoke(d, new object[] { chromoseome });

                }
                else
                {
                    Graphics gr = panel1.CreateGraphics();
                    gr.Clear(panel1.BackColor);
                    Pen pen = new Pen(Color.White, 2);
                    SolidBrush redBrush = new SolidBrush(Color.Red);
                    foreach (var item in g.nodes)
                    {
                        Node n = item as Node;
                        gr.FillEllipse(redBrush, (int)n.point.x, (int)n.point.y, 10, 10);

                    }

                    var genes = chromoseome.GetGenes();
                    var first = (int)genes[0].Value;
                    Node n1;
                    Node n2;
                    for (int i = 1; i < genes.Length; i++)
                    {
                        n1 = g.nodes[(int)genes[i - 1].Value] as Node;
                        n2 = g.nodes[(int)genes[i].Value] as Node;

                        gr.DrawLine(Pens.Red, (int)n1.point.x + 5, (int)n1.point.y + 5, (int)n2.point.x + 5, (int)n2.point.y + 5);

                    }
                    n1 = g.nodes[first] as Node;
                    n2 = g.nodes[(int)genes.Last().Value] as Node;
                    gr.DrawLine(Pens.Red, (int)n1.point.x + 5, (int)n1.point.y + 5, (int)n2.point.x + 5, (int)n2.point.y + 5);
                    //gr.draw(redBrush, (int)n.point.x, (int)n.point.y, 10, 10);


                    //this.label7.Text = text;
                }
            }
            catch (ObjectDisposedException)
            {
                m_ga.Stop();
            }

        }
        public void SetText(string text)
        {

            if (this.label7.InvokeRequired)
            {
                try
                {
                    SetTextCallback d = new SetTextCallback(SetText);

                    this.label7.Invoke(d, new object[] { text });
                }
                catch(ObjectDisposedException )
                {
                    m_ga.Stop();
                }
            }
            else
            {

                this.label7.Text = text;
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {

                m_ga.Stop();

            
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //m_ga.Stop();
        }
    }
}
