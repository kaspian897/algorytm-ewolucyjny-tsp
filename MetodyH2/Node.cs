﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodyH2
{
    class Node
    {
        public class Point
        {
            public double x;
            public double y;
            public Point(double x, double y)
            {
                this.x = x;
                this.y = y;
            }
        }
        public int id;
        public Point point;
        public HashSet<Edge> edges;
        //public int ID{set;get;}
        public Node(int id,Point point)
        {
            edges = new HashSet<Edge>();
            this.id = id;
            this.point = point;
        }
        
    }
}
