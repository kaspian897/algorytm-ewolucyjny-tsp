﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace MetodyH2
{
    class Graph
    {
        public ArrayList nodes;
        public HashSet<Edge> edges;
        public Graph(int nodes, int edges,int width,int height)
        {
            this.nodes = new ArrayList();
            this.edges = new HashSet<Edge>();

            Random rnd = new Random();
            
            for (int i = 0; i < nodes; i++)
            {
                int x = rnd.Next(0, width);
                int y = rnd.Next(0, height);

                this.nodes.Add(new Node(i, new Node.Point(x,y)));
            }

            LinkedList<Edge> allEdges = new LinkedList<Edge>();
            for(int i=0; i < nodes; i++)
            {
                for(int j = i+1; j < nodes; j++)
                {
                        allEdges.AddLast(new Edge(i, j));

                }
            }
            int k = 0;
            var result = new LinkedListNode<Edge>[allEdges.Count];
            for (var node = allEdges.First; node != null; node = node.Next)
            {
                int j = rnd.Next(k + 1);
                if (k != j)
                    result[k] = result[j];
                result[j] = node;
                k++;
            }

            //Console.WriteLine();
            for (int i = 0; i < Math.Min(edges, allEdges.Count); i++)
            {

                // int index=rnd.Next(0, allEdges.Count);
                //Edge edge=allEdges.ElementAt(index);
                Edge edge = result[i].Value;
                ((Node)this.nodes[edge.start]).edges.Add(edge);
                ((Node)this.nodes[edge.end]).edges.Add(edge);
                this.edges.Add(edge);
               // allEdges.Remove(edge);
            }
        }
    }
}
